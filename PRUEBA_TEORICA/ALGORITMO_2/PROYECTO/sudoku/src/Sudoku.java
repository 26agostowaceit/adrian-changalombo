public class Sudoku {

    public static void main(String[] args) {
        // // set initial boards
        int[][] board1 = {
                {1, 2, 4, 3},
                {3, 4, 2, 1},
                {2, 1, 3, 4},
                {4, 3, 1, 2}
        };

        int[][] board2 = {
                {1, 2, 4, 3},
                {3, 3, 2, 1},
                {2, 1, 3, 4},
                {4, 3, 2, 1}
        };

        System.out.println(isValidSudoku(board1));
        System.out.println(isValidSudoku(board2));
    }


    /**
     * Validates if a 4x4 Sudoku board is valid or not
     *
     * @param board The Sudoku board to be validated
     * @return True if the board is valid, false otherwise
     */
    private static boolean isValidSudoku(int[][] board) {
        // // validate rows and columns
        for (int i = 0; i < 4; i++) {
            if (!isValidRow(board, i) || !isValidColumn(board, i)) {
                return false;
            }
        }

        // validate sub-grids
        for (int i = 0; i < 4; i += 2) {
            for (int j = 0; j < 4; j += 2) {
                if (!isValidSubGrid(board, i, j)) {
                    return false;
                }
            }
        }

        return true;
    }


    /**
     * Validates if a row of the Sudoku board is valid
     *
     * @param board Sudoku board
     * @param row   Row index
     * @return True if the row is valid, false otherwise
     */
    private static boolean isValidRow(int[][] board, int row) {
        boolean[] seen = new boolean[4];
        for (int num : board[row]) {
            if (num < 1 || num > 4 || seen[num - 1]) {
                return false;
            }
            seen[num - 1] = true;
        }
        return true;
    }


    /**
     * Validates if a column of the Sudoku board is valid
     *
     * @param board Sudoku board
     * @param col   Column index
     * @return True if the column is valid, false otherwise
     */
    private static boolean isValidColumn(int[][] board, int col) {
        boolean[] seen = new boolean[4];
        for (int i = 0; i < 4; i++) {
            int num = board[i][col];
            if (num < 1 || num > 4 || seen[num - 1]) {
                return false;
            }
            seen[num - 1] = true;
        }
        return true;
    }


    /**
     * Validates if a sub-grid of the Sudoku board is valid
     *
     * @param board    Sudoku board
     * @param startRow Starting row index of the sub-grid
     * @param startCol Starting column index of the sub-grid
     * @return True if the sub-grid is valid, false otherwise
     */
    private static boolean isValidSubGrid(int[][] board, int startRow, int startCol) {
        boolean[] seen = new boolean[4];

        // loop through each cell in the 2x2 sub-grid
        for (int i = startRow; i < startRow + 2; i++) {
            for (int j = startCol; j < startCol + 2; j++) {
                int num = board[i][j];

                // validate out of range
                if (num < 1 || num > 4 || seen[num - 1]) {
                    return false;
                }
                seen[num - 1] = true;
            }
        }
        return true;
    }

}