import java.util.Random;

public class Main {
    public static void main(String[] args) {
        // // Create the board with given size and set penalty positions (index 0)
        int[] board = generateBoard(18);
        board[3] = -1;
        board[9] = -2;

        int playerPosition = 0;
        int round = 1;
        Random random = new Random();


        // // start game
        while (playerPosition < board.length - 1) {
            // roll a dice
            int roll = random.nextInt(6) + 1;
            System.out.println("ROUND #" + round);
            System.out.println("Roll: " + roll);

            // calculate new position
            int newPosition = playerPosition + roll;
            if (newPosition >= board.length) {
                System.out.println("WINNER");
                break;
            }

            // // apply penalty
            playerPosition = newPosition;
            if (board[playerPosition] == -1) {
                playerPosition -= 1;
            } else if (board[playerPosition] == -2) {
                playerPosition -= 2;
            }

            // // display updated board
            displayBoard(board, playerPosition);
            round++;
        }
    }


    /**
     * Generates a game board with the given size
     *
     * @param size The size of the game board
     * @return An array representing the game board
     */
    private static int[] generateBoard(int size) {
        return new int[size];
    }

    /**
     * Displays the game board with the player's position
     *
     * @param board The actual game board
     * @param playerPosition The player's current position
     */
    private static void displayBoard(int[] board, int playerPosition) {
        for (int i = 0; i < board.length; i++) {
            if (i == playerPosition) {
                // display payer's position
                System.out.print("x ");
            } else if (board[i] == -1) {
                // display penalty -1
                System.out.print("-1 ");
            } else if (board[i] == -2) {
                // display penalty -2
                System.out.print("-2 ");
            } else {
                // display empty cell
                System.out.print("_ ");
            }
        }

        System.out.println();
    }
}
